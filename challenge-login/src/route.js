const express = require("express");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const { addHours, addMinutes } = require("date-fns");
const db = require("./db/users.json");
const { isAuthenticated } = require("./middleware")
const route = express.Router();

route.post(
  "/auth/login", 
(req, res, next) => {
  const { username, password } = req.body;

  if (!username && !password) {
    return res
    .status(412)
    .json({ error: "ga ada bro! masukin username sama password ya."})
  }

//Check username  
  const user = db.find((user) => user.email === username);
  console.log(password, user);
  if (!user) {
    return res
    .status(404)
    .json({error: "ga ketemu ni."})
  }
// compare password
const isValid = bcrypt.compareSync(password, user.password);
if (!isValid) {
   return res.status(404).json({error: "kagak ketemu ni."})
};

req.user = user;

next();
},
(req, res,) => {
  const { username, email } = req.user;

  const loggedInAt = Date.now();
  const accesToken = jwt.sign(
    {
    loggedInAt,
    user: { username, email },
    }, process.env.JWT_SECRET
  );
  const accesTokenExpiredAt = addHours(loggedInAt, 1);
  const refreshToken = jwt.sign(
    {
    loggedInAt
  }, process.env.JWT_SECRET
  );
  const refreshTokenExpiredAt = addMinutes(loggedInAt, 50);

  res.json({
    accesToken,
    accesTokenExpiredAt,
    refreshToken,
    refreshTokenExpiredAt,
  });
  }
);

route.get("/me", isAuthenticated, (req, res) =>{
  req.log.info("Start getting user")
  const user = db.find((user) => user.username === req.session.user.username);
  res.json(user);
});
route.get("/users", isAuthenticated, (req, res) => {
  res.json(db);
});

module.exports = route;