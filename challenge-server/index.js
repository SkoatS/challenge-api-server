const express = require('express');
const app = express();
const path = require('path');


app.set("view engine", "ejs");

app.use("/views/asset", express.static(path.join(__dirname, "/views/assets")));
app.use(express.static(__dirname));


app.get("/", function(req, res){
    res.render(__dirname + "/views/home.ejs")
});

app.get("/work", function(req, res){
  res.render(__dirname + `/views/work.ejs`)
});

app.get("/contact-us", function(req, res){
  res.render(__dirname + `/views/contact-us.ejs`)
});

app.get("/about-us", function(req, res){
  res.render(__dirname + `/views/about-us.ejs`)
});

app.get("/game-bgk", function(req, res){
    res.render(__dirname + "/views/game-bgk.ejs")
});

app.listen(8010, function() {
    console.log("App running at http://localhost:8010")
});